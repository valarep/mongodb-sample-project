﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDbSampleProject.Model.Dao;
using MongoDbSampleProject.Model.Classes;

namespace MongoDbSampleProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EmployeeDao dao = new EmployeeDao();


            /*
            dao.Create(new Employee()
            {
                FirstName = "Bernard",
                LastName = "DUPONT",
                BirthDate = new DateTime(2000, 01, 01),
                Email = "bernard.dupont@gmail.com",
                Salary = 1978.5M,
                Service = "Informatique"
            });
            */

            List<Employee> employees = dao.Get();
            Employee employee = employees.FirstOrDefault();

            /*
            employee.LastName = "DUPUIS";
            dao.Update(employee.Id, employee);
            employee = dao.Get(employee.Id);
            */

            // bool result = dao.Remove(employee);
            bool result = dao.Remove("000000000000000000000000");

            //MessageBox.Show(employee.FirstName + " " + employee.LastName);
            MessageBox.Show("result:" + result);


        }
    }
}
