﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDbSampleProject.Model.Classes
{
    public class Employee
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("BirthDate")]
        public DateTime BirthDate { get; set; }

        [BsonElement("Email")]
        public string Email { get; set; }

        [BsonElement("Service")]
        public string Service { get; set; }

        [BsonElement("Salary")]
        public decimal Salary { get; set; }
    }
}
