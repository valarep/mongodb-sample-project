﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDbSampleProject.Model.Dal;
using MongoDbSampleProject.Model.Classes;

namespace MongoDbSampleProject.Model.Dao
{
    class EmployeeDao : AbstractDao
    {
        private readonly IMongoCollection<Employee> _employees;

        public EmployeeDao() : base()
        {
            _employees = Database.GetCollection<Employee>("Employees");
        }

        public List<Employee> Get()
        {
            return _employees.Find(employee => true).ToList();
        }

        public Employee Get(string id)
        {
            return _employees.Find(employee => employee.Id == id).FirstOrDefault();
        }

        public void Create(Employee document)
        {
            _employees.InsertOne(document);
        }

        public bool Update(string id, Employee replacement)
        {
            ReplaceOneResult result = _employees.ReplaceOne(employee => employee.Id == id, replacement);
            return result.IsAcknowledged && result.ModifiedCount == 1;
        }

        public bool Remove(Employee document)
        {
            DeleteResult result = _employees.DeleteOne(employee => employee.Id == document.Id);
            return result.IsAcknowledged && result.DeletedCount == 1;
        }

        public bool Remove(string id)
        {
            DeleteResult result = _employees.DeleteOne(employee => employee.Id == id);
            return result.IsAcknowledged && result.DeletedCount == 1;
        }
    }
}
