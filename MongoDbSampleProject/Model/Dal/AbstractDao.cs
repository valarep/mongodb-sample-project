﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MongoDB.Driver;
using MongoDbSampleProject.Model.Classes;

namespace MongoDbSampleProject.Model.Dal
{
    abstract class AbstractDao
    {
        public MongoClient Client { get; private set; }
        public IMongoDatabase Database { get; private set; }

        public AbstractDao()
        {
            string mongoUrl = ConfigurationManager.AppSettings["ConnectionString"];
            string dbname = ConfigurationManager.AppSettings["Database"];
            Client = new MongoClient(new MongoUrl(mongoUrl));
            Database = Client.GetDatabase(dbname);
        }
    }
}
